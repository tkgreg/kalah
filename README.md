#Grava Hal (aka Kalah)

###Requirements

* Java 1.8
* Maven 3.2.2


###How to build game

Run from project folder:

```
mvn clean install
```

###How to run

```
java -jar ./target/kalah.jar
```

1. Open link [http://localhost:8080](http://localhost:8080)
1. Enter username
1. Start new game
1. Provide game link to your friend and game code to connect
1. Have fun :)

Live [server](http://kalah.grigorii.tk)

###Game Rules
####Game Play
The player who begins with the first move picks up all the stones in anyone of his own six pits, and sows the stones on to the right, one in each of the following pits, including his own Kalah. No stones are put in the opponents' Kalah. If the player's last stone lands in his own Kalah, he gets another turn. This can be repeated several times before it's the other player's turn.

####Capturing Stones
During the game the pits are emptied on both sides. Always when the last stone lands in an own empty pit, the player captures his own stone and all stones in the opposite pit (the other players' pit) and puts them in his own Kalah.