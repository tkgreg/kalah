package tkgreg.mankala;

import org.junit.Test;
import tkgreg.mankala.model.GameSession;
import tkgreg.mankala.model.Pit;
import tkgreg.mankala.model.Player;
import tkgreg.mankala.service.GameService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static tkgreg.mankala.model.GameSession.CONNECTED_PLAYER_GH_POSITION;
import static tkgreg.mankala.model.GameSession.INITIATOR_GH_POSITION;

/**
 * Created by greg on 16/04/15.
 */
public class GameTest {


    private GameService gameService = new GameService();

    @Test
    public void mainGameTest() {

        GameSession gameSession = gameService.startNewGame("Greg");
        gameSession.setNextTurnByInitiator(true);

        gameService.connect(gameSession.getId(), "Jack");
        gameService.turn(gameSession.getId(), gameSession.getInitiator(), 0);

        List<Integer> result = gameSession.getPits().stream().map(Pit::getStones).collect(Collectors.toList());
        assertEquals(Arrays.asList(0, 7, 7, 7, 7, 7, 1, 6, 6, 6, 6, 6, 6, 0), result);
        assertEquals(gameSession.isNextTurnByInitiator(), true);

        gameService.turn(gameSession.getId(), gameSession.getInitiator(), 3);
        result = gameSession.getPits().stream().map(Pit::getStones).collect(Collectors.toList());
        assertEquals(Arrays.asList(0, 7, 7, 0, 8, 8, 2, 7, 7, 7, 7, 6, 6, 0), result);
        assertEquals(gameSession.isNextTurnByInitiator(), false);

        gameService.turn(gameSession.getId(), gameSession.getOpponent(), 8);
        result = gameSession.getPits().stream().map(Pit::getStones).collect(Collectors.toList());
        assertEquals(Arrays.asList(1, 8, 7, 0, 8, 8, 2, 7, 0, 8, 8, 7, 7, 1), result);
        assertEquals(gameSession.isNextTurnByInitiator(), true);

        gameService.turn(gameSession.getId(), gameSession.getInitiator(), 5);
        result = gameSession.getPits().stream().map(Pit::getStones).collect(Collectors.toList());
        assertEquals(Arrays.asList(2, 8, 7, 0, 8, 0, 3, 8, 1, 9, 9, 8, 8, 1), result);
        assertEquals(gameSession.isNextTurnByInitiator(), false);

        gameService.turn(gameSession.getId(), gameSession.getOpponent(), 11);
        result = gameSession.getPits().stream().map(Pit::getStones).collect(Collectors.toList());
        assertEquals(Arrays.asList(3, 9, 8, 1, 9, 1, 3, 8, 1, 9, 9, 0, 9, 2), result);
        assertEquals(gameSession.isNextTurnByInitiator(), true);

        gameService.turn(gameSession.getId(), gameSession.getInitiator(), 5);
        result = gameSession.getPits().stream().map(Pit::getStones).collect(Collectors.toList());
        assertEquals(Arrays.asList(3, 9, 8, 1, 9, 0, 4, 8, 1, 9, 9, 0, 9, 2), result);
        assertEquals(gameSession.isNextTurnByInitiator(), true);
    }

    @Test
    public void testEndGamePlain() {
        Player player = new Player("Greg");
        GameSession gameSession = new GameSession(player);

        Player opponent = new Player("Jack");

        Game.connectToGame(opponent, gameSession);

        //prepare gameSession state for test

        gameSession.getPits().get(0).setStones(0);
        gameSession.getPits().get(1).setStones(0);
        gameSession.getPits().get(2).setStones(0);
        gameSession.getPits().get(3).setStones(0);
        gameSession.getPits().get(4).setStones(0);
        gameSession.getPits().get(5).setStones(1);
        gameSession.getPits().get(6).setStones(26);
        gameSession.getPits().get(7).setStones(5);
        gameSession.getPits().get(8).setStones(1);
        gameSession.getPits().get(9).setStones(6);
        gameSession.getPits().get(10).setStones(4);
        gameSession.getPits().get(11).setStones(6);
        gameSession.getPits().get(12).setStones(3);
        gameSession.getPits().get(13).setStones(20);

        gameSession.setNextTurnByInitiator(true);

        Game.makeTurn(player, 5, gameSession);

        assertEquals(true, gameSession.isGameFinished());
        assertEquals(27, gameSession.getPits().get(INITIATOR_GH_POSITION).getStones());
        assertEquals(45, gameSession.getPits().get(CONNECTED_PLAYER_GH_POSITION).getStones());

    }

    @Test
    public void testEndGameWithCapture() {
        Player player = new Player("Greg");
        GameSession gameSession = new GameSession(player);

        Player opponent = new Player("Jack");
        Game.connectToGame(opponent, gameSession);

        //prepare gameSession state for test

        gameSession.getPits().get(0).setStones(0);
        gameSession.getPits().get(1).setStones(0);
        gameSession.getPits().get(2).setStones(0);
        gameSession.getPits().get(3).setStones(0);
        gameSession.getPits().get(4).setStones(8);
        gameSession.getPits().get(5).setStones(0);
        gameSession.getPits().get(6).setStones(35);
        gameSession.getPits().get(7).setStones(1);
        gameSession.getPits().get(8).setStones(0);
        gameSession.getPits().get(9).setStones(2);
        gameSession.getPits().get(10).setStones(2);
        gameSession.getPits().get(11).setStones(1);
        gameSession.getPits().get(12).setStones(4);
        gameSession.getPits().get(13).setStones(19);

        gameSession.setNextTurnByInitiator(false);

        Game.makeTurn(opponent, 7, gameSession);

        assertEquals(true, gameSession.isGameFinished());
        assertEquals(35, gameSession.getPits().get(INITIATOR_GH_POSITION).getStones());
        assertEquals(37, gameSession.getPits().get(CONNECTED_PLAYER_GH_POSITION).getStones());

    }

    @Test
    public void testGameShouldNotEnd() {
        Player player = new Player("Greg");
        GameSession gameSession = new GameSession(player);

        Player opponent = new Player("Jack");
        Game.connectToGame(opponent, gameSession);

        //prepare gameSession state for test

        gameSession.getPits().get(0).setStones(0);
        gameSession.getPits().get(1).setStones(2);
        gameSession.getPits().get(2).setStones(3);
        gameSession.getPits().get(3).setStones(0);
        gameSession.getPits().get(4).setStones(1);
        gameSession.getPits().get(5).setStones(2);
        gameSession.getPits().get(6).setStones(32);
        gameSession.getPits().get(7).setStones(0);
        gameSession.getPits().get(8).setStones(0);
        gameSession.getPits().get(9).setStones(0);
        gameSession.getPits().get(10).setStones(1);
        gameSession.getPits().get(11).setStones(0);
        gameSession.getPits().get(12).setStones(1);
        gameSession.getPits().get(13).setStones(30);

        gameSession.setNextTurnByInitiator(false);

        Game.makeTurn(opponent, 10, gameSession);

        assertEquals(false, gameSession.isGameFinished());
        assertEquals(32, gameSession.getPits().get(INITIATOR_GH_POSITION).getStones());
        assertEquals(33, gameSession.getPits().get(CONNECTED_PLAYER_GH_POSITION).getStones());

    }
}
