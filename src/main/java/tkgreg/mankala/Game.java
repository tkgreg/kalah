package tkgreg.mankala;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tkgreg.mankala.model.GameSession;
import tkgreg.mankala.model.Pit;
import tkgreg.mankala.model.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static tkgreg.mankala.model.GameSession.CONNECTED_PLAYER_GH_POSITION;
import static tkgreg.mankala.model.GameSession.INITIATOR_GH_POSITION;

/**
 * Created by greg on 16/04/15.
 */
public class Game {

    private static Logger logger = LoggerFactory.getLogger(Game.class);

    public static GameSession makeTurn(Player player, int pitNumber, GameSession gameSession) {
        logger.debug("[{}] Move arrived from User={} pit=", gameSession.getId(), player, pitNumber);
        boolean turnState = gameSession.isNextTurnByInitiator();
        List<Pit> pits = gameSession.getPits();
        logger.debug("[{}] Move from pit {}", gameSession.getId(), pitNumber);
        logger.debug("[{}] Before turn", gameSession.getId());
        if (logger.isDebugEnabled()) {
            dumpPits(pits);
        }
        boolean isOpponent = gameSession.isOpponent(player);
        int positionToSkip = (isOpponent) ? INITIATOR_GH_POSITION : CONNECTED_PLAYER_GH_POSITION;
        int kahalPosition = (isOpponent) ? CONNECTED_PLAYER_GH_POSITION : INITIATOR_GH_POSITION;
        Pit kahal = gameSession.getPits().get(kahalPosition);
        Pit startPit = pits.get(pitNumber);
        int stonesInPit = startPit.getStones();
        int position = startPit.getNum();
        Pit currentPit = pits.get(position);
        currentPit.addStones(1);
        startPit.setStones(0);
        for (int i = 0; i < stonesInPit; i++) {
            currentPit = getPositionToAddStone(currentPit, gameSession);
            if (currentPit.getNum() == positionToSkip) {
                currentPit = getPositionToAddStone(currentPit, gameSession);
            }
            currentPit.addStones(1);
        }

        //capture stones detection
        if (!currentPit.isKalah() && currentPit.getStones() == 1 && isPlayerPit(currentPit, isOpponent)) {
            captureStones(currentPit, kahal, gameSession);
        }

        //whose next turn
        if (currentPit.getNum() != kahalPosition) {
            gameSession.setNextTurnByInitiator(!turnState);
        }

        //detect end of the game
        detectEndOfTheGame(gameSession);

        logger.debug("[{}] After turn", gameSession.getId());
        if (logger.isDebugEnabled()) {
            dumpPits(gameSession.getPits());
        }
        return gameSession;
    }

    private static void detectEndOfTheGame(GameSession gameSession) {
        boolean player1NoMoreMoves = gameSession.getPits().subList(0, 5).stream().allMatch(pit -> pit.getStones() == 0);
        boolean player2NoMoreMoves = gameSession.getPits().subList(7, 13).stream().allMatch(pit -> pit.getStones() == 0);
        boolean noMoreMove = player1NoMoreMoves || player2NoMoreMoves;

        if (noMoreMove) {
            if (player2NoMoreMoves) {
                int sum = gameSession.getPits().subList(0, 6).stream().mapToInt(Pit::getStones).sum();
                gameSession.getPits().subList(0, 5).forEach(pit -> pit.setStones(0));
                gameSession.getPits().get(INITIATOR_GH_POSITION).addStones(sum);
            } else {
                int sum = gameSession.getPits().subList(7, 13).stream().mapToInt(Pit::getStones).sum();
                gameSession.getPits().subList(7, 13).forEach(pit -> pit.setStones(0));
                gameSession.getPits().get(CONNECTED_PLAYER_GH_POSITION).addStones(sum);
            }
            logger.debug("[{}] Game finished", gameSession.getId());
            gameSession.setGameFinished(true);
        }
    }

    private static void dumpPits(List<Pit> pits) {
        System.out.print(pits.get(CONNECTED_PLAYER_GH_POSITION).getStones() + "GH|");
        List<Pit> lst = new ArrayList<>(pits.subList(7, 13));
        Collections.reverse(lst);
        lst.forEach(p -> System.out.print(p.getStones() + "|"));
        System.out.print("\n");
        System.out.print("    ");
        pits.subList(0, 6).forEach(p -> System.out.print(p.getStones() + "|"));
        System.out.print(pits.get(INITIATOR_GH_POSITION).getStones() + "GH\n");
    }

    private static boolean isPlayerPit(Pit currentPit, boolean isOpponent) {
        if (currentPit.getNum() >= 0 && currentPit.getNum() < 6 && !isOpponent) {
            return true;
        } else if (currentPit.getNum() > 6 && isOpponent) {
            return true;
        }
        return false;
    }


    private static void captureStones(Pit currentPit, Pit kahal, GameSession gameSession) {
        logger.debug("[{}] Stones captured currentPit={}", gameSession.getId(), currentPit);
        kahal.addStones(1);
        Pit oppositePit = gameSession.getPits().get(currentPit.getOppositeNum());
        kahal.addStones(oppositePit.getStones());
        oppositePit.setStones(0);
        currentPit.setStones(0);
    }

    private static Pit getPositionToAddStone(Pit currentPit, GameSession gameSession) {
        if (currentPit.getNum() + 1 == 14) {
            return gameSession.getPits().get(0);
        } else {
            return gameSession.getPits().get(currentPit.getNum() + 1);
        }
    }


    public static GameSession connectToGame(Player opponent, GameSession gameSession) {
        if (!gameSession.hasOpponent()) {
            gameSession.setOpponent(opponent);
            return gameSession;
        }
        throw new IllegalStateException("Game already has opponent !!!");

    }


}
