package tkgreg.mankala;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by greg on 16/04/15.
 */
@EnableAutoConfiguration
@ComponentScan(basePackages = {"tkgreg.mankala"})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}

