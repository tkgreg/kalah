package tkgreg.mankala.dto;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by greg on 18/04/15.
 */
public class CallbackDto implements Serializable{

    public static final String OPPONENT_ONLINE = "onOpponentEvent";
    public static final String MY_TURN = "onMyTurnEvent";

    public String gameId;
    public UUID playerId;
    public String event;


}
