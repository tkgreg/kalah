package tkgreg.mankala.dto;

import java.io.Serializable;

/**
 * Created by greg on 18/04/15.
 */
public class ConnectDto implements Serializable{
    public String gameId;
    public String opponentName;

}
