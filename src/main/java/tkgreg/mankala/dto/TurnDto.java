package tkgreg.mankala.dto;

import tkgreg.mankala.model.Player;

import java.io.Serializable;

/**
 * Created by greg on 18/04/15.
 */
public class TurnDto implements Serializable {
    public String gameId;
    public Player player;
    public int pitNum;
}
