package tkgreg.mankala.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tkgreg.mankala.GameNotFoundException;
import tkgreg.mankala.dto.CallbackDto;
import tkgreg.mankala.dto.ConnectDto;
import tkgreg.mankala.dto.TurnDto;
import tkgreg.mankala.model.GameSession;
import tkgreg.mankala.service.GameService;

import java.util.Optional;

/**
 * Created by greg on 16/04/15.
 */
@RestController
@RequestMapping("/api")
public class GameController {

    @Autowired
    private GameService gameService;


    @RequestMapping(value = "/start", method = RequestMethod.POST)
    public GameSession startNewGame(@RequestBody String playerName) {
        return gameService.startNewGame(playerName);
    }

    @RequestMapping(value = "/connect", method = RequestMethod.POST)
    public GameSession connect(@RequestBody ConnectDto connectDto) {
        Optional<GameSession> gameSessionOptional = gameService.connect(connectDto.gameId, connectDto.opponentName);
        if (gameSessionOptional.isPresent()) {
            return gameSessionOptional.get();
        } else {
            throw new GameNotFoundException();
        }
    }

    @RequestMapping(value = "/turn", method = RequestMethod.POST)
    public GameSession turn(@RequestBody TurnDto turnDto) {
        return gameService.turn(turnDto.gameId, turnDto.player, turnDto.pitNum);
    }

    @RequestMapping(value = "/callback", method = RequestMethod.POST)
    public GameSession callback(@RequestBody CallbackDto callback) {
        switch (callback.event) {
            case CallbackDto.OPPONENT_ONLINE: {
                return gameService.isOpponentOnline(callback.gameId, callback.playerId).orElse(null);
            }
            case CallbackDto.MY_TURN: {
                return gameService.getGameSessionById(callback.gameId);
            }
        }
        return null;
    }

}
