package tkgreg.mankala.model;

import java.io.Serializable;

/**
 * Created by greg on 16/04/15.
 */
public class Pit implements Serializable {

    private int num;
    private int stones = 6;
    private int oppositeNum;
    private boolean kalah;

    public Pit(int num, int oppositeNum) {
        this.num = num;
        this.oppositeNum = oppositeNum;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getOppositeNum() {
        return oppositeNum;
    }

    public void setOppositeNum(int oppositeNum) {
        this.oppositeNum = oppositeNum;
    }

    public int getStones() {
        return stones;
    }

    public void setStones(int stones) {
        this.stones = stones;
    }

    public void addStones(int amount) {
        stones += amount;
    }

    public boolean isKalah() {
        return kalah;
    }

    public void setKalah(boolean kalah) {
        this.kalah = kalah;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pit pit = (Pit) o;

        return num == pit.num;

    }

    @Override
    public int hashCode() {
        return num;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Pit{");
        sb.append("num=").append(num);
        sb.append(", stones=").append(stones);
        sb.append(", oppositeNum=").append(oppositeNum);
        sb.append(", kalah=").append(kalah);
        sb.append('}');
        return sb.toString();
    }
}
