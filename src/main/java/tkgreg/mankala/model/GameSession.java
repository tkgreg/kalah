package tkgreg.mankala.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by greg on 16/04/15.
 */
public class GameSession implements Serializable {

    public static int INITIATOR_GH_POSITION = 6;
    public static int CONNECTED_PLAYER_GH_POSITION = 13;
    private String id;
    private Player initiator;
    private Player opponent;
    private List<Pit> pitList = new ArrayList<>(14);
    private boolean nextTurnByInitiator;
    private boolean gameFinished;

    public GameSession(Player initiator) {
        id = UUID.randomUUID().toString().substring(0, 4);
        this.initiator = initiator;
        setStartState();
    }

    private void setStartState() {
        nextTurnByInitiator = new Random().nextBoolean();
        for (int i = 0; i < 14; i++) {
            pitList.add(new Pit(i, 12 - i));
        }
        pitList.get(INITIATOR_GH_POSITION).setKalah(true);
        pitList.get(INITIATOR_GH_POSITION).setStones(0);
        pitList.get(CONNECTED_PLAYER_GH_POSITION).setKalah(true);
        pitList.get(CONNECTED_PLAYER_GH_POSITION).setStones(0);
    }

    public boolean isOpponent(Player player) {
        return opponent.equals(player);
    }

    public String getId() {
        return id;
    }

    public List<Pit> getPits() {
        return pitList;
    }

    public Player getInitiator() {
        return initiator;
    }

    public boolean hasOpponent() {
        return opponent != null;
    }

    public Player getOpponent() {
        return opponent;
    }

    public void setOpponent(Player opponent) {
        this.opponent = opponent;
    }

    public boolean isNextTurnByInitiator() {
        return nextTurnByInitiator;
    }

    public void setNextTurnByInitiator(boolean nextTurnByInitiator) {
        this.nextTurnByInitiator = nextTurnByInitiator;
    }

    public boolean isGameFinished() {
        return gameFinished;
    }

    public void setGameFinished(boolean gameFinished) {
        this.gameFinished = gameFinished;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GameSession{");
        sb.append("id=").append(id);
        sb.append(", initiator=").append(initiator);
        sb.append(", opponent=").append(opponent);
        sb.append(", pitList=").append(pitList);
        sb.append('}');
        return sb.toString();
    }
}
