package tkgreg.mankala.model;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by greg on 16/04/15.
 */
public class Player implements Serializable {

    public Player() {

    }

    public Player(String name) {
        this.name  = name;
        this.id = UUID.randomUUID();
    }

    public UUID id;
    public String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return !(name != null ? !name.equals(player.name) : player.name != null);

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Player{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
