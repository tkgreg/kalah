package tkgreg.mankala.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tkgreg.mankala.Game;
import tkgreg.mankala.model.GameSession;
import tkgreg.mankala.model.Player;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by greg on 18/04/15.
 */
@Service
public class GameService {

    private static Logger logger = LoggerFactory.getLogger(GameService.class);

    private ConcurrentHashMap<String, GameSession> gamesList = new ConcurrentHashMap<>();

    public GameSession startNewGame(String initiatorName) {
        Player player = new Player(initiatorName);
        GameSession gameSession = new GameSession(player);
        gamesList.put(gameSession.getId(), gameSession);
        logger.debug("New game started by player={}", player);
        return gameSession;
    }

    public Optional<GameSession> connect(String gameId, String opponentName) {
        if (gamesList.containsKey(gameId)) {
            GameSession gameSession = gamesList.get(gameId);
            Player player = new Player(opponentName);
            Game.connectToGame(player, gameSession);
            logger.debug("New user={} connected to game={}", player, gameId);
            return Optional.of(gameSession);
        } else {
            logger.debug("GameId={} not founded", gameId);
            return Optional.empty();
        }
    }

    public GameSession turn(String gameId, Player player, int pitNumber) {
        GameSession gameSession = gamesList.get(gameId);
        return Game.makeTurn(player, pitNumber, gameSession);
    }

    public GameSession getGameSessionById(String gameSession) {
        return gamesList.get(gameSession);
    }

    public Optional<GameSession> isOpponentOnline(String gameId, UUID playerId) {
        GameSession gameSession = gamesList.get(gameId);
        return (gameSession.hasOpponent() && gameSession.getInitiator().id.equals(playerId)) ? Optional.of(gameSession) : Optional.empty();
    }

}
