package tkgreg.mankala;

/**
 * Created by greg on 18/04/15.
 */

public class GameNotFoundException extends RuntimeException {


    public GameNotFoundException() {
        super("Game not found");
    }
}
