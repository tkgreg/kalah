/**
 * Created by greg on 17/04/15.
 */
kalahApp.controller('gameController', function ($scope, $http, $timeout) {

    var player = {};
    var initiator = false;

    $scope.messageBox = '';
    $scope.currentUserKalah = [];
    $scope.currentUserName = '';
    $scope.opponentUserName = '';
    $scope.opponentKalah = [];
    $scope.opponentUserPits = [];
    $scope.currentUserPits = [];
    $scope.gameId = '';
    $scope.gameStarted = false;
    buttonsNotBlocked = false;

    var waitOpponent = function () {
        $http.post('/api/callback', {
            gameId: $scope.gameId,
            playerId: player.id,
            event: 'onOpponentEvent'
        }).success(function (gameSession) {
            if (gameSession) {
                if (gameSession.opponent) {
                    $scope.opponentUserName = gameSession.opponent.name;
                }
                detectWhosTurn(gameSession);
            } else {
                $timeout(waitOpponent, 1000);
            }
        });
    };

    var waitForTurn = function () {
        $http.post('/api/callback', {
            gameId: $scope.gameId,
            playerId: player.id,
            event: 'onMyTurnEvent'
        }).success(function (gameSession) {
            if (gameSession) {
                detectWhosTurn(gameSession);
            } else {
                $timeout(waitForTurn, 1000);
            }
        });
    };

    var detectWhosTurn = function (gameSession) {
        if (gameSession.gameFinished) {
            $scope.messageBox = "Game finished!";
            updateBoard(gameSession);
        } else {
            if (gameSession.nextTurnByInitiator == initiator) {
                $scope.messageBox = 'Your turn!';
                updateBoard(gameSession);
                buttonsNotBlocked = true;
            } else {
                $scope.messageBox = 'Opponent turn!';
                updateBoard(gameSession);
                buttonsNotBlocked = false;
                waitForTurn();
            }
        }
    };

    var createDumbArray = function (total) {
        var range = [];
        for (var i = 0; i < total; i++) {
            range.push(i);
        }
        return range;
    };


    var updateBoard = function (gameSession) {
        $timeout(function () {
            if (initiator) {
                $scope.currentUserName = gameSession.initiator.name;
                if (gameSession.opponent) {
                    $scope.opponentUserName = gameSession.opponent.name;
                }
                $scope.currentUserKalah = createDumbArray(gameSession.pits[6].stones);
                $scope.currentUserPits = gameSession.pits.slice(0, 6);
                $scope.opponentKalah = createDumbArray(gameSession.pits[13].stones);
                $scope.opponentUserPits = gameSession.pits.slice(7, 13).reverse();
            } else {
                $scope.currentUserName = gameSession.opponent.name;
                $scope.currentUserKalah = createDumbArray(gameSession.pits[13].stones);
                $scope.currentUserPits = gameSession.pits.slice(7, 13);
                $scope.opponentUserName = gameSession.initiator.name;
                $scope.opponentKalah = createDumbArray(gameSession.pits[6].stones);
                $scope.opponentUserPits = gameSession.pits.slice(0, 6).reverse();
            }
        }, 100);
    };

    var startGameSessionCallback = function (gameSession) {
        console.log("Game: " + gameSession.id);
        $scope.gameStarted = true;
        initiator = true;
        player = gameSession.initiator;
        $scope.gameId = gameSession.id;
        updateBoard(gameSession);
        $scope.messageBox = 'Waiting opponent';
        waitOpponent();
    };

    var connectCallback = function (gameSession) {
        player = gameSession.opponent;
        $scope.gameStarted = true;
        $scope.gameId = gameSession.id;
        detectWhosTurn(gameSession);
    };

    $scope.start = function () {
        if (!$scope.userName || $scope.userName == '') {
            $scope.messageBox = 'Please enter your name';
        } else {
            $scope.messageBox = '';
            $http.post('/api/start', $scope.userName).success(startGameSessionCallback);
        }
    };

    $scope.connect = function () {
        if (!$scope.userName || $scope.userName == '') {
            $scope.messageBox = 'Please enter your name';
        } else {
            $scope.messageBox = '';
            $http.post('/api/connect', {
                gameId: $scope.gameId,
                opponentName: $scope.userName
            }).success(connectCallback).error(function (error) {
                $scope.messageBox = error.message;
            });
        }
    };

    $scope.makeTurn = function (pit) {
        if (buttonsNotBlocked && pit.stones != 0) {
            buttonsNotBlocked = true;
            $http.post('/api/turn', {
                gameId: $scope.gameId,
                player: player,
                pitNum: pit.num
            }).success(detectWhosTurn);
            pit.stones = 0;
        } else if (pit.stones == 0) {
            $scope.messageBox = "Can't move from this pit";
        }
    };

});